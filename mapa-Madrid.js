d3.json('Practica/practica_airbnb.json')
    .then((featureCollection) => {
        // definimos los datos iniciales a graficar del barrio de Palacio
        bedrooms = ["BedRooms 0", "BedRooms 1", "BedRooms 2", "BedRooms 3", "BedRooms 4"];
        totalBedrooms = [79, 684, 221, 78, 20]
        drawMap(featureCollection);
        drawChart(featureCollection, "Cortes");
    });


/**
 * 
 * drawMap@param {*} featureCollection 
 * 
 *  
 */

function drawMap(featureCollection) {
    // Definimos el numero de rangos
    noRangos = 10;

    // Iteramos los los datos GeoJson para sacar el precio promedio de todos los barrios
    console.log(featureCollection.features);
    var features = featureCollection.features;
    precioAvgTotal = [];
    for (var i = 0; i < features.length; i++) {
        //console.log(featureCollection.features[i]);
        precioAvg = featureCollection.features[i].properties.avgprice;
        if (!isNaN(precioAvg)) {
            precioAvgTotal.push(precioAvg);
        }
    };
    var precioAvgMin = Math.min.apply(null, precioAvgTotal);
    var precioAvgMax = Math.max.apply(null, precioAvgTotal);
    var maxMenosMin = precioAvgMax - precioAvgMin;
    var incremento = maxMenosMin / noRangos;

    //Asignamos los valores de los colores de Tableay a un array
    colorArray = d3.schemeTableau10


    // Generamos la lista de etiquetas hasta un maximo de 10 por los colores a representar
    // Para los rangos de precios
    var etiquetasPreciosColoresMap = new Map();
    var rangoEtiquetasPrecios = [];
    var precioAvgMin = Math.min.apply(null, precioAvgTotal);
    precioInicial = precioAvgMin;
    for (var i = 0; i < noRangos; i++) {
        minEtiqueta = precioInicial.toString();
        precioTope = precioInicial + incremento;
        maxEtiqueta = precioTope.toString();
        rangoEtiquetasPrecios.push(minEtiqueta+" - "+maxEtiqueta);
        etiquetasPreciosColoresMap.set(minEtiqueta+" - "+maxEtiqueta, colorArray[i]);
        precioInicial = precioTope;
    }
    //console.log(rangoEtiquetasPrecios);
    //console.log(etiquetasPreciosColoresMap);

    // Armamos los array de rangos de precios y colores
    etiquetasMenu = [];
    coloresMenu = [];
    for (let [key, value] of etiquetasPreciosColoresMap) {
        etiquetasMenu.push(key);
        coloresMenu.push(value);
    }
    //console.log(coloresMenu);
    //console.log(etiquetasMenu);


    // asignamos un rango de precio promedio a cada ciudad
    for (var i = 0; i < features.length; i++) {
        featureCollection.features[i].properties.color = '';
        featureCollection.features[i].properties.rangoPrecio = '';
    }

    minEtiqueta = precioAvgMin.toString();
    maxEtiqueta = "";
    for (var i = 0; i < features.length; i++) {
        //console.log(featureCollection.features[i]);
        precioAvg = featureCollection.features[i].properties.avgprice;
        indiceColor = -1;
        if (!isNaN(precioAvg)) {

            for (let [key, value] of etiquetasPreciosColoresMap) {
                cadena = key;
                posiDash = cadena.search("-");
                //console.log(posiDash);
                
                numero1 = parseFloat(cadena.substr(0,posiDash-1));
                //console.log(numero1);
                
                numero2 = parseFloat(cadena.substr(posiDash+2));
                //console.log(numero2);
                
                if(precioAvg >= numero1 && precioAvg <= numero2){
                    featureCollection.features[i].properties.color = value;
                    featureCollection.features[i].properties.rangoPrecio = key;
                } 
            }
        } else {
            // Cuando el precio promedio es NaN se asigna un color NEGRO
            featureCollection.features[i].properties.color = "#000000";
        }
    };

    var width = 800;
    var height = 500;

    var svg = d3.selectAll('div')
        .select('#mapaDiv')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr("transform", "translate(0, 10)");

    var center = d3.geoCentroid(featureCollection); //Encontrar la coordenada central del mapa (de la featureCollection)
    //var center_area = d3.geoCentroid(featureCollection.features[0]); //Encontrar la coordenada central de un area. (de un feature)

    //console.log(center)

    //to translate geo coordinates[long,lat] into X,Y coordinates.
    var projection = d3.geoMercator()
        .fitSize([width, height], featureCollection) // equivalente a  .fitExtent([[0, 0], [width, height]], featureCollection)
        //.scale(1000)
        //Si quiero centrar el mapa en otro centro que no sea el devuelto por fitSize.
        .center(center) //centrar el mapa en una long,lat determinada
        .translate([width / 2, height / 2]) //centrar el mapa en una posicion x,y determinada

    //console.log(projection([long,lat]))

    //Para crear paths a partir de una proyección 
    var pathProjection = d3.geoPath().projection(projection);

    var createdPath = svg.selectAll('path')
        .data(features)
        .enter()
        .append('path')
        .attr('d', (d) => pathProjection(d))
        .attr("opacity", function(d, i) {
            d.opacity = 1
            return d.opacity
        })
        .on("mouseover", handleMouseOver)
        .on("mouseout", handleMouseOut);

    createdPath.on('click', function(event, d) {
        //d.opacity = d.opacity ? 0 : 1;
        //d3.select(this).attr('opacity', d.opacity);
        //console.log(d.properties.avgprice);
        //console.log(d.properties.name);
        drawChart(featureCollection, d.properties.name);
    })

    var scaleColor = d3.scaleOrdinal(d3.schemeTableau10);
    createdPath.attr('fill', (d) => d.properties.color);
    // console.log(scaleColor);


    //Creacion de una leyenda
    var nblegend = 10;
    var widthRect = (width / nblegend) - 2;
    var heightRect = 15;

    var scaleLegend = d3.scaleLinear()
        .domain([0, nblegend])
        .range([0, width]);

    var legend = svg.append("g")
        .selectAll("rect")
        .data(coloresMenu)
        .enter()
        .append("rect")
        .attr("width", widthRect)
        .attr("height", heightRect)
        .attr("x", (d, i) => scaleLegend(i)) // o (i * (widthRect + 2)) //No haria falta scaleLegend
        .attr("fill", (d) => d)
        .attr("transform", "translate(0, 470)");;

    var text_legend = svg.append("g")
        .selectAll("text")
        //.data(d3.schemeTableau10)
        .data(etiquetasMenu)
        .enter()
        .append("text")
        .attr("x", (d, i) => scaleLegend(i)) // o (i * (widthRect + 2))
        .attr("y", heightRect * 2.5)
        .text((d) => d)
        .attr("font-size", 12)
        .attr("transform", "translate(0, 445)")
        .style("font-weight", "bold");;
    
        //Creamos el elemento Tooltip
    var tooltip = d3
    .select("div")
    .append("div")
    .attr("class", "tooltip")
    .style("position", "absolute") //Para obtener la posicion correcta sobre los circulos
    .style("pointer-events", "none") //Para evitar el flicker
    .style("visibility", "hidden")
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "1px")
    .style("border-radius", "5px")
    .style("padding", 5);

    function handleMouseOver(event, d) {
        d3.select(this)
        .transition()
        .duration(1000)
        .attr("fill", "#FFFF00");
        
        tooltip
        .transition()
        .duration(200)
        .style("visibility", "visible")
        .style("opacity", .9)
        .style("left", event.pageX + 20 + "px")
        .style("top", event.pageY - 30 + "px")
        .text(`Barrio : ${d.properties.name}, Precio : ${d.properties.avgprice}`)
        .style("color", "#FF00FF");

    }
    
    function handleMouseOut(event, d) {
        d3.select(this)
        .transition()
        .duration(200)
        .attr("fill", d.properties.color);
        //.attr("r", (d) => ratio);
    
        /* console.log(("#text" + d.idx))
        d3.select(("#text" + d.idx)).style(
            "opacity", 0);
        console.log(d);*/
    
        tooltip.transition().duration(200).style("visibility", "hidden");
        // .style("opacity", .9)
    }
  
}

/**
 * 
 * 
 * Funcion para desplegar el grafico de barras asociado al barrio de Madrid
 * 
 * 
 */


function drawChart(featureCollection, barrio) {


    // Validamos las camas disponibles en cada barrio
    var featuresBarrio = featureCollection.features;
    var dataCamasBarrio = [];
    for (var i = 0; i < featuresBarrio.length; i++) {
        if (featuresBarrio[i].properties.name == barrio){
            dataCamasBarrio = featuresBarrio[i].properties.avgbedrooms;
        }
    };

    let claves = Object.keys(dataCamasBarrio); 
    // console.log(claves);
    var bedRoomsStr = [];
    for(let i=0; i< claves.length; i++){
        let clave = claves[i];
        bedRoomsStr.push("BedRooms "+dataCamasBarrio[clave].bedrooms);
    };
    //console.log(bedRoomsStr);      

    var height = 300;
    var width = 800;
    var marginbottom = 100;
    var margintop = 50;

    d3.selectAll('#SVG_ID').remove();

    var svg = d3.select('div')
    .append('svg')
    .attr("id","SVG_ID")
    .attr('width', width)
    .attr('height', height + marginbottom + margintop)
    .append("g")
    .attr("transform", "translate(30," + margintop + ")");

    var domainX = dataCamasBarrio.map(function (d) {
        return d.bedrooms;
    });
      
    //Creacion de escala de colores.
    var scaleColor = d3.scaleOrdinal().domain(domainX).range(d3.schemeTableau10);

    //Creacion de escalas
    var xscale = d3.scaleBand()
    .domain(dataCamasBarrio.map(function(d) {
        return d.bedrooms;
    }))
    .range([0, width])
    .padding(0.2);

    var yscale = d3.scaleLinear()
    .domain([0, d3.max(dataCamasBarrio, function(d) {
        return d.total;
    })])
    .range([height, 0]);

    //Creación de eje X
    var xaxis = d3.axisBottom(xscale);
    var yaxis = d3.axisLeft(yscale);

    //Creacion de los rectangulos
    var rect = svg
    .selectAll('rect')
    .data(dataCamasBarrio)
    .enter()
    .append('rect')
    .attr("fill", function (d) {
        return scaleColor(d.bedrooms);
    });

    //.attr("fill", "#93CAAE");

    rect.attr('class', (d) => {
        if (d.total > 10) {
            return 'rectwarning';
        }
    });

    rect
    .attr("x", function(d) {
        return xscale(d.bedrooms);
    })
    .attr('y', d => {
        return yscale(0)
    })
    .attr("width", xscale.bandwidth())
    .attr("height", function() {
        return height - yscale(0); //Al cargarse los rectangulos tendran una altura de 0 
    })
    .on("mouseover", function() {
        d3.select(this).attr("class", "").attr("fill", "yellow")
    })
    .on("mouseout", function() {
        d3.select(this).attr("fill", "#93CAAE")
            .attr('class', (d) => {
                if (d.total > 10) {
                    return 'rectwarning';
                }
            })
    });

/*
    svg.append("text")
    .attr("x", (width / 2))             
    .attr("y", 0 - (70 / 2))
    .attr("text-anchor", "middle")  
    .style("font-size", "16px") 
    .style("text-decoration", "underline")  
    .text("Value vs Date Graph");

*/

    // Titulo  x axis
    svg
    .append("text")
    .attr("transform", "translate(" + width / 2 + " ," + (height / 2 + 180) + ")")
    .style("text-anchor", "middle")
    .text("Cuartos con habitacion privada en el barrio : " + barrio.toUpperCase())
    .style("font-weight", "bold");

    rect
    .transition() //transición de entrada
    .ease(d3.easeBounce)
    .duration(1000)
    .delay(function(d, i) {
        console.log(i);
        return (i * 300)
    })
    .attr('y', d => {
        return yscale(d.total)
    })
    .attr("height", function(d) {
        return height - yscale(d.total); //Altura real de cada rectangulo.
    });


    //Añadimos el texto correspondiente a cada rectangulo.
    var text = svg.selectAll('text')
    .data(dataCamasBarrio)
    .enter()
    .append('text')
    .text(d => d.total)
    .attr("x", function(d) {
        return xscale(d.bedrooms) + xscale.bandwidth() / 2;
    })
    .attr('y', d => {
        return yscale(d.total)
    })
    .style("opacity", 0);

    //Transicción de entrada en el texto.
    text
    .transition()
    .ease(d3.easeBounce)
    .duration(500)
    .delay(d3.max(dataCamasBarrio, function(d, i) {
        return i;
    }) * 300 + 1000)
    .style("opacity", 1);

    //Añadimos el eje X
    svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(xaxis);

    
    //Añadimos eje Y
    svg.append("g").attr("transform", "translate(0, 0)").call(yaxis);

}
